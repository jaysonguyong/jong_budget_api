const mongoose = require('mongoose')

const recordSchema = mongoose.Schema({
	categoryName:{ 
		type: String,
		required:[true, "Category Name in record is required"]
	},
	categoryType:{
		type: String,
		required:[true, "Category type in record is required"] 
	},
	isActive:{
		type:Boolean,
		default: true
	},
	amount:{ 
		type:Number,
		default:0
	},
	runningBalance:{
		type:Number,
		default:0
	},
	description:{
		type: String,
		required:[true, "Description is required"]
	}, 
	users:
	[
		{
			userId:{
				type:String,
				required:[true, "Record id is required"]
			}

		}
	],
	createdOn:{
		type:Date,
		default:Date.now()
	} 

})

module.exports = mongoose.model('record', recordSchema)