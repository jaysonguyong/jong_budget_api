const RecordController = require('../controllers/record')
const express = require('express')
const router = express.Router()
const auth = require('../auth')

router.post('/', auth.verify,(req, res)=>{
	const params={
		userId: auth.decode(req.headers.authorization).id,
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType,
		amount: req.body.amount,
		runningBalance: req.body.runningBalance, 
		description: req.body.description
	}
	RecordController.addRecord(params).then(result => res.send(result)) 
})

router.get('/', (req, res)=>{
	RecordController.getAll().then(result => res.send(result))
})
router.get('/user/:id', auth.verify, (req, res)=>{ 

	RecordController.getUserRecords(req.params).then(result => res.send(result))
})


router.get('/ids', (req, res) => {
    RecordController.getAllIds().then(result => res.send(result))
})

// router.delete('/:id', auth.verify, (req, res)=>{
// 	const params = {
// 		id: req.params.id,
// 		userId: auth.decode(req.headers.authorization).id
// 	}
// 	RecordController.archive(params).then(result=>res.send(result))
// })

router.delete('/:recordId', auth.verify, (req, res) => {
	// console.log(auth.decode(req.headers.authorization).id)
	const params = {
		recordId: req.params.recordId,
		userId: auth.decode(req.headers.authorization).id
	}
	RecordController.toggleDelete(params).then(result => res.send(result))
})



module.exports = router