const User = require('../models/user')
const Record = require('../models/record')
const bcrypt= require('bcrypt')
const {OAuth2Client} = require('google-auth-library')
const clientId = '719590584713-gn89bo3715u1ibnoc2fpm7k7avlem4f4.apps.googleusercontent.com'
const auth = require('../auth')

module.exports.register = (params)=>{
	const user = new User({
			email:params.email,
			password:bcrypt.hashSync(params.password, 10)
		})
	return user.save()
	.then((user, err)=>{
		return (err) ? false : true
	})
}	

module.exports.login = (params) => {
    return User.findOne({email: params.email})
    .then(user => {
        if(user === null){
            return false
        }
        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
        if(isPasswordMatched){
    
            return {
                accessToken: auth.createAccessToken(user.toObject())
            }
        }else{
            return false
        }
    })
}

module.exports.getDetails = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.password = undefined
        return user
    })
}

module.exports.getAllIds = () => {
    return User.find()
    .then((users, err) => {
        if (err) return false
        const ids = users.map(user => user._id)
        return ids
    })
}

module.exports.getAll = (params)=>{
    return User.findById(params)
    .then((user, err)=>{
        if (err) return false
        return user.categories
    })
}

module.exports.editBalance = (params)=>{
    return User.findByIdAndUpdate(params.id, {
        balance:params.balance},{new:true})
    .then(user => {
        console.log(user)
        return user
    })

}

module.exports.verifyGoogleTokenId = async (tokenId) =>{
    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
    /* if the verification process is successful, the google account detail shall contain by the 
    data variable, and data variable has a property called 'payload', which contains the 
    information about the user*/   
    if(data.payload.email_verified === true) {
        console.log(data.payload.email_verified)
        /*
            Asynchronous chain, wala siyang kinaiba sa User.findOne().then()
            the .exec() is same as the .then()
        */
        const user = await User.findOne({email: data.payload.email}).exec()
        if(user !== null){
            //Check if the user has a loginType = 'google', return an accesstoken
            if(user.loginType === 'google'){
                return {
                    accessToken: auth.createAccessToken(user.toObject())
                }
            } else {
                return {error: 'login-type-error'}
            }
        }else{
            // return an error msg
            const newUser = new User({
                firstName:data.payload.given_name,
                lastName:data.payload.family_name,
                email:data.payload.email,
                loginType: 'google'
            })

            return newUser.save().then((user, err)=>{
                console.log(user)
                return {
                    accessToken: auth.createAccessToken(user.toObject())
                }
            })
        }
    } else {
        // if the email is failed to verified
        return {
            error: 'google-auth-error'
        }
    } 
}

module.exports.getUserRecords = (params) =>{
    return User.findById(params.userId).then(user => {
        const recordIds = user.records.map(record => record.recordId)
        return Record.find().where('_id').in(recordIds).then(records => {
            return records.filter(record => record.isActive === true)
        })
    })
}